# JK3 Linux Dedicated Server Management Script

Jedi Knight 3: Jedi Academy Linux Dedicated Server Management Script.

## Installation

1. Copy `jk3server` into a directory in your `$PATH` (e.g. `/usr/local/bin`) and set permissions.
```sh
cp jk3server /usr/local/bin
chown jads: /usr/local/bin/jk3server
chmod ug+x /usr/local/bin/jk3server
```

2. Copy `jk3ds_start.sh` into the same directory as your Jedi Academy dedicated server binary (e.g. `/jads/`) and set permissions. 
```sh
cp jk3ds_start.sh /jads/
chown jads: /usr/local/bin/jk3ds_start.sh
chmod u+x /usr/local/bin/jk3ds_start.sh
```

## Usage

Start the JK3 dedicated server.
```sh
jk3server start
```

Stop the JK3 dedicated server.
```sh
jk3server stop
```

Restart the JK3 dedicated server.
```sh
jk3server restart
```

Check JK3 dedicated server status.
```sh
jk3server status
```

Follow live JK3 dedicated server log.
```sh
jk3server tail
```

## Acknowledgements

Raz0r