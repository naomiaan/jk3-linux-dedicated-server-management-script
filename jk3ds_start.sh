#!/bin/bash
# jk3ds_start.sh
# Jedi Knight 3: Jedi Academy Linux Dedicated Server Auto-Restart Launcher
# Author: Raz0r @ https://jkhub.org/forums/topic/2089-restart-script/
# Version: 0.1.0
#

SERVER_DIR="/jads"
START_OPTIONS="+set dedicated 2 +set fs_game japlus +exec server.cfg"

echo "Starting Jedi Knight: Jedi Academy Server..."
echo "Use CTRL+C to terminate the server or CTRL+A+D to detach screen session"
status=1
while [ $status -ne 0 ]
do
  cd $SERVER_DIR
  ./linuxjampded $START_OPTIONS >jk3ds-server.log 2>&1
  status=$?

  if [ $status -ne 0 ]
  then
    date=`/bin/date`
    echo "JK3DS server crashed with status "$status" at "$date
  fi
done
echo "Server exited gracefully."